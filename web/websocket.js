var WebSocket = require('ws');
var mysql = require('promise-mysql');

wss = new WebSocket.Server({ noServer: true });

wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(data) {
        var jsonData;
        try {
            jsonData = JSON.parse(data);
        }
        catch(err) {
            console.log("can't read JSON");
        }
        if (jsonData && jsonData.goal) {

            storeGoal(jsonData.goal,ws)
            .then( success => {
                if (success) {
                    wss.clients.forEach(function each(client) {
                        if (client.readyState === WebSocket.OPEN) {
                            client.send(data);
                        }
                    });
                } else {
                    console.log("store failed");
                }
            })
            .catch( e => {
                console.log("turns out it threw an error");
            })
            
        } else if (jsonData && jsonData.broadcast) {
            wss.clients.forEach(function each(client) {
                if (client.readyState === WebSocket.OPEN) {
                    client.send(data);
                }
            });
        }
    });
});

var storeGoal = async function(goal,ws) {
    try {
        var con = await mysql.createConnection({
            host: process.env.DB_HOST,
            user: process.env.MYSQL_USER,
            password: process.env.MYSQL_PASSWORD,
            database: "indoorkids"
        });

        let response = await con.query('SELECT id FROM matches WHERE ended_at IS NULL ORDER BY created_at DESC LIMIT 1');

        if (response.length == 0) {
            ws.send(JSON.stringify({
                error: {
                    code: "no-match",
                    message: "No match in progress"
                }
            }))
            return false;
        }

        con.beginTransaction();

        var query = await con.format(`INSERT INTO goals (scorer_id,assist_id,goalkeeper_id,match_id,team_id,og_id) VALUES (?,?,?,?,?,?)`,[goal.scorer,goal.assist,goal.keeper,goal.match,goal.team,goal.own_goal]);
        response = await con.query(query);

        con.commit();
        await con.end();

        return true;
    } catch(e) {

        con.rollback();
        await con.end();
        console.log("error encountered",e);
        return false;

    }
}

module.exports = wss;