var express = require('express');
var router = express.Router();
var mysql = require('promise-mysql');

/* GET home page. */
router.get('/', function(req, res, next) {
  
  loadIndex(req, res, next);

});

var loadIndex = async function(req, res, next) {
  try {

    var con = await mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: "indoorkids"
    });

    let response = await con.query('SELECT id FROM matches WHERE ended_at IS NULL ORDER BY created_at DESC LIMIT 1');

    if (response.length !== 0) {
      res.redirect('/live-match');
      return;
    }

    goals = await con.query(`
      select count(goals.id) as goals,
      (select count(team_players.id) from team_players where team_players.player_id = players.id) as games_played,
      (count(goals.id)/(select count(team_players.id) from team_players where team_players.player_id = players.id)) as goals_per_game,
      players.name
      from goals
      inner join players
      on goals.scorer_id = players.id
      group by goals.scorer_id
      order by goals_per_game desc;`);

    assists = await con.query(`
      select count(goals.id) as assists,
      (select count(team_players.id) from team_players where team_players.player_id = players.id) as games_played,
      (count(goals.id)/(select count(team_players.id) from team_players where team_players.player_id = players.id)) as assists_per_game,
      players.name
      from goals
      inner join players
      on goals.assist_id = players.id
      group by goals.assist_id
      order by assists_per_game desc;`);

    await con.end();

    res.render('index', { 
      title: 'Indoor Kids',
      goals: goals,
      assists: assists,
    });


  } catch(e) {

    console.log("error encountered",e);
    await con.end();
    res.status(500);
    res.send('loading index failed');

  }
  
}

module.exports = router;
