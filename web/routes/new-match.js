var express = require('express');
var router = express.Router();
var mysql = require('promise-mysql');

/* GET home page. */
router.get('/', function(req, res, next) {
  
  loadIndex(req, res, next);

});

var loadIndex = async function(req, res, next) {
  try {

    var con = await mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: "indoorkids"
    });

    let response = await con.query('SELECT id FROM matches WHERE ended_at IS NULL ORDER BY created_at DESC LIMIT 1');

    if (response.length !== 0) {
      res.redirect('/live-match');
      return;
    }

    response = await con.query('SELECT id as value, name FROM players');

    await con.end();

    res.render('new-match', { 
      title: 'New Match',
      players: response,
      playersJSON: JSON.stringify(response)
    });


  } catch(e) {

    console.log("error encountered",e);
    await con.end();
    res.status(500);
    res.send('loading new-match failed');

  }
  
}

module.exports = router;
