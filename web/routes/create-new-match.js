var express = require('express');
var router = express.Router();
var mysql = require('promise-mysql');

/* POST mnew match players. */
router.post('/', function(req, res, next) {
  doPost(req, res, next);
});

async function doPost(req, res, next){
  try {

    var con = await mysql.createConnection({
      host: process.env.DB_HOST,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: "indoorkids"
    });

    con.beginTransaction();

    let response = await con.query('SELECT id FROM matches WHERE ended_at IS NULL ORDER BY created_at DESC LIMIT 1');

    if (response.length !== 0) {
      con.rollback();
      res.redirect('/live-match');
      return;
    }

    response = await con.query('INSERT INTO matches () VALUES ()');
    var match_id = response.insertId;

    let insertQuery = 'INSERT INTO teams (number,match_id) VALUES (1,?)';
    let query = await con.format(insertQuery,[match_id]);
    response = await con.query(query);
    var team_one_id = response.insertId;

    insertQuery = 'INSERT INTO teams (number,match_id) VALUES (2,?)';
    query = await con.format(insertQuery,[match_id]);
    response = await con.query(query);
    var team_two_id = response.insertId;

    var team_players = [];
    var teamPlayerInsertQuery = 'INSERT INTO team_players (team_id,player_id,match_id) VALUES ';

    for (const player of req.body.team_one) {
      if (!player.id) {
        let insertQuery = 'INSERT INTO players (name) VALUES (?)';
        let query = await con.format(insertQuery,[player.name]);
        let response = await con.query(query);
        player.id = response.insertId;
      }
      team_players.push(team_one_id,player.id,match_id);
      teamPlayerInsertQuery += '(?,?,?),'
    }

    for (const player of req.body.team_two) {
      if (!player.id) {
        let insertQuery = 'INSERT INTO players (name) VALUES (?)';
        let query = await con.format(insertQuery,[player.name]);
        let response = await con.query(query);
        player.id = response.insertId;
      }
      team_players.push(team_two_id,player.id,match_id);
      teamPlayerInsertQuery += '(?,?,?),'
    }

    teamPlayerInsertQuery = teamPlayerInsertQuery.substr(0,teamPlayerInsertQuery.length-1); //Remove the final trailing comma

    query = await con.format(teamPlayerInsertQuery,team_players);
    response = await con.query(query);

    con.commit();
    await con.end();

    res.send('match created successfuly');

  } catch(e) {

    con.rollback();
    await con.end();
    console.log("error encountered",e);
    res.status(500);
    res.send('match creation failed');

  }
}

module.exports = router;
