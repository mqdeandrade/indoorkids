var express = require('express');
var router = express.Router();
var mysql = require('promise-mysql');

/* GET home page. */
router.get('/', function(req, res, next) {
  
  if (req.query.id) {
    endMatch(req, res, next,req.query.id)
  } else {
    res.status(400);
    res.send('please supply match id');
  }
  
});

var endMatch = async function(req, res, next,match_id) {
  try {

    var con = await mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: "indoorkids"
    });
  
    con.beginTransaction();

    let response = await con.query('SELECT id FROM matches WHERE ended_at IS NULL ORDER BY created_at DESC LIMIT 1');
    var current_match = response[0];

    if (current_match.id !== Number(match_id)) {
      con.rollback();
      res.status(400);
      res.send('selected match is not in progress');
      return;
    }
  
    let insertQuery = 'UPDATE matches SET ended_at = NOW() WHERE id = ?';
    let query = await con.format(insertQuery,[match_id]);
    response = await con.query(query);

    con.commit();
    await con.end();

    res.send('match ended successfuly');


  } catch(e) {

    con.rollback();
    await con.end();
    console.log("error encountered",e);
    res.status(500);
    res.send('ending match failed');

  }
  
}

module.exports = router;
