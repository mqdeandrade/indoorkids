var express = require('express');
var router = express.Router();
var mysql = require('promise-mysql');

/* GET home page. */
router.get('/', function(req, res, next) {
  
  liveMatch(req, res, next);

});

var liveMatch = async function(req, res, next) {
  try {

    var con = await mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: "indoorkids"
    });

    let response = await con.query('SELECT id FROM matches WHERE ended_at IS NULL ORDER BY created_at DESC LIMIT 1');

    if (response.length == 0) {
      res.redirect('/');
      return;
    }

    let players = await con.query(`SELECT players.name, players.id, teams.number as team_number, m1.id as match_id,
      (SELECT COUNT(goals.id)
      FROM goals
      WHERE goals.scorer_id = players.id
      AND goals.match_id = m1.id) as goals,
      (SELECT COUNT(goals.id)
      FROM goals
      WHERE goals.assist_id = players.id
      AND goals.match_id = m1.id) as assists
      FROM team_players
      INNER JOIN matches m1
      ON team_players.match_id = m1.id
      AND m1.ended_at IS NULL
      LEFT OUTER JOIN matches m2 ON (team_players.match_id = m2.id AND m2.ended_at IS NULL AND 
          (m1.created_at < m2.created_at OR m1.created_at = m2.created_at AND m1.created_at < m2.created_at))
      INNER JOIN teams
      ON teams.id = team_players.team_id
      INNER JOIN players
      ON team_players.player_id = players.id;`);

    var team_one = players.filter(function(player, index, arr){
      return player.team_number === 1;
    });

    var team_two = players.filter(function(player, index, arr){
      return player.team_number === 2;
    });

    let scores = await con.query(`SELECT
      (SELECT COUNT(goals.id)
      FROM goals
      WHERE goals.team_id = teams.number
      AND goals.match_id = m1.id) as score
      FROM matches m1
      LEFT OUTER JOIN matches m2 ON (m2.ended_at IS NULL AND 
          (m1.created_at < m2.created_at OR m1.created_at = m2.created_at AND m1.created_at < m2.created_at))
      INNER JOIN teams
      ON teams.match_id = m1.id
      WHERE m1.ended_at IS NULL
      ORDER BY teams.number ASC;`);

    await con.end();
  
    score_one = scores[0].score;
    score_two = scores[1].score;

    res.render('live-match', { 
      title: 'Live Match',
      team_one: team_one,
      team_two: team_two,
      match_id: players[0].match_id,
      score_one: score_one,
      score_two: score_two,
      playersJSON: JSON.stringify(players)
    });


  } catch(e) {

    console.log("error encountered",e);
    await con.end();
    res.status(500);
    res.send('ending match failed');

  }
  
}

module.exports = router;
